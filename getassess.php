<?php
include("includes/common.php");
$class=$_REQUEST['q'];
$sid =$_REQUEST['id'];
$year =$_REQUEST['year'];
$stid=$_REQUEST['stid'];
//fitness assessment

$ft=$db->prepare("select * from fitness_assessment where academic_year=:year and st_id =:sid");
$ft->bindParam(':year', $year);
$ft->bindParam(':sid', $sid);
$ft->execute();
$ftrows = $ft->fetchAll();
$count1 = $ft->rowCount();
if($count1 >0)
{
$term =  "First";
$first=$db->prepare("select * from fitness_assessment where academic_year=:year and term=:term and st_id =:sid");
$first->bindParam(':year', $year);
$first->bindParam(':term', $term);
$first->bindParam(':sid', $sid);
$first->execute();
$firstdata = $first->fetch();

$term2 =  "Second";
$second=$db->prepare("select * from fitness_assessment where academic_year=:year and term=:term and st_id =:sid");
$second->bindParam(':year', $year);
$second->bindParam(':term', $term2);
$second->bindParam(':sid', $sid);
$second->execute();
$seconddata = $second->fetch();
}






//student details for skill assessment
$ut=$db->prepare("select * from assess_term where accademic_year=:year and st_id =:sid");
$ut->bindParam(':year', $year);
$ut->bindParam(':sid', $sid);
$ut->execute();
$utrows = $ut->fetch();
$count = $ut->rowCount();

$id_assess_term= $utrows ['id_assess_term'];

//unit questions
$assign=$db->prepare("select * from unit_assign");
$assign->execute();
$assignrows = $assign->fetchAll();
/*echo "<pre>";
print_r($assignrows);
echo "</pre>";*/


?>
<div id="exTab2"  style="border:1px solid #CCC;width:auto;background-color:#fff;color:#000;margin-left:10%;margin-right:10%;">	


	<ul class="nav nav-tabs" style="background-color:#006dcc">
				<li class="active">
		<a  href="#1" data-toggle="tab" ><input type="button" class="btn btn-primary" value="Skill"  style="font-size:20px"></a>
				</li>
				<li><a href="#2" data-toggle="tab"><input type="button" class="btn btn-primary" value="Fitness"  style="font-size:20px"></a>
				</li>
			</ul>

		<div class="tab-content " style="font-size:14px">
		 <div class="tab-pane active" id="1"><br>
		 <?php if($count >0){?>
		  <table border=1 width="100%">
		<tr><td align="center" colspan="2"><h3>Student ID : <?php echo $stid;?></h3></td></tr>
		<tr><td align="center" colspan="2"><h4>Academic Year : <?php echo $year;?></h4></td></tr>
		<?php
			for($i=0;$i<count($assignrows);$i++)
			{
				$classes=explode(",",$assignrows[$i]['classes']);
				if(in_array($class, $classes)){
				$id_units= $assignrows[$i]['id_units'];

				  //list of units
					$ut1=$db->prepare("select * from units where id_units =:sid");
					$ut1->bindParam(':sid', $id_units);
					$ut1->execute();
					$utrows1 = $ut1->fetch();

					$ut2=$db->prepare("select * from unit_questions where id_units =:sid");
					$ut2->bindParam(':sid', $id_units);
					$ut2->execute();
					$utrows2 = $ut2->fetchAll();
			?>
				<tr><td align="left" colspan="2"><h4><?php echo $utrows1['unit_name'];?> </h4></td></tr>
				<tr><td align="left" colspan="2"><h6>Objective : <?php echo $utrows1['objective'];?> </h6></td></tr>
				<tr><td width="80%">
			<?php 	for($j=0;$j<count($utrows2);$j++){?>
				<table  border=1 width="100%"><tr><td width="50%"><?php echo $utrows2[$j]['questions'];?> </td><td width="50%">
				<?php $id_ques= $utrows2[$j]['id_unit_ques'];
				$assign1=$db->prepare("select * from unit_assess where id_unit_ques = :id_ques and id_assess_term = :id_assess_term");
				$assign1->bindParam(':id_ques', $id_ques);
				$assign1->bindParam(':id_assess_term', $id_assess_term);
				$assign1->execute();
				$assignrows1 = $assign1->fetch();
				echo "&nbsp;".$assignrows1['score'];
				?>
				</td></tr></table>
			<?php }?></td><td><img src="../admin/img/units/<?php echo $utrows1['image'];?>" width="120" height="100"></td></tr>
			<?php		

				}
			}

		?>			  <input type="hidden"  name="class" id="class" value="<?php echo $class;?>"  />  
		<tr><td colspan="2">&nbsp;</td></tr> 
		<tr><td colspan="2">
		<table border=1 width="100%">		
		<tr><td width="40%"><h4>Overall Grade </h4></td><td width="40%"> 
		<?php echo "<h4>&nbsp;".$utrows['overall_grade']."</h4>";?> 
		</td><td  align="center"  width="20%"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showskillgraph('graph/linegraph_skill.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>);"  alt="view graph" width="20" height="20" border="0"></td></tr></table>
		</td></tr> 	
		</table>
<table width="100%" border=1><tr><td>&nbsp;&nbsp;</td><td> <b>AD</b> - Advanced </td>  <td><b> EV</b> - Evolved   </td> </tr><tr><td>&nbsp;&nbsp;</td><td>     <b>NO </b>- Novice </td><td>  <b>NA</b> - Not Applicable</td></tr></table>
		  <?php }else { echo "No assessesment";}?>

		 </div>
		 <div class="tab-pane" id="2"><br>
		    <?php if($count1 >0){?>
		  <table border=1 width="100%">
		<tr><td align="center" colspan="4"><h3>Student ID : <?php echo $stid;?></h3></td></tr>
		<tr><td align="center" colspan="4"><h4>Academic Year : <?php echo $year;?></h4></td></tr>
		<tr><td>&nbsp;</td><td align="center" ><h5>Term : <?php echo $firstdata['term'];?></h5></td><td align="center" ><h5>Term : <?php echo  $seconddata['term'];?></h5></td><td align="center"><h5>Graph </h5></td></tr>
		
		<tr><td align="center">Height </td><td align="center"><?php echo $firstdata['height'];?> <?php echo $firstdata['height_measure'];?></td>
		<td align="center"><?php echo $seconddata['height'];?> <?php echo $seconddata['height_measure'];?></td> <td  align="center"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showgraph('graph/new.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>,'height');" alt="view graph" width="20" height="20" border="0"></td></tr>

		<tr><td align="center">Weight</td><td align="center"> <?php echo $firstdata['weight'];?> <?php echo $firstdata['weight_measure'];?></td>
		<td align="center"><?php echo $seconddata['weight'];?> <?php echo $seconddata['weight_measure'];?></td><td  align="center"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showgraph('graph/new.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>,'weight');" alt="view graph" width="20" height="20" border="0"></td></tr>

		<tr><td align="center">BMI</td><td align="center"><?php echo round($firstdata['BMI'],2);?></td><td  align="center"><?php echo round($seconddata['BMI'],2);?></td><td  align="center"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showgraph('graph/new.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>,'BMI');" alt="view graph" width="20" height="20" border="0"></td></tr>

		<tr><td align="center">Endurance (Sit-ups) </td><td align="center"><?php echo $firstdata['endurance'];?></td><td  align="center"><?php echo $seconddata['endurance'];?></td><td  align="center"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showgraph('graph/new.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>,'endurance');"  alt="view graph" width="20" height="20" border="0"></td></tr>

		<tr><td align="center">Upper_body (Medicine Ball Throw)</td><td align="center"><?php echo $firstdata['upper_body'];?></td><td align="center"><?php echo $seconddata['upper_body'];?></td><td  align="center"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showgraph('graph/new.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>,'upper_body');"  alt="view graph" width="20" height="20" border="0"></td></tr>

		<tr><td align="center">Lower_body (Standing Broad Jump) </td><td align="center"> <?php echo $firstdata['lower_body'];?></td><td align="center"><?php echo $seconddata['lower_body'];?></td><td  align="center"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showgraph('graph/new.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>,'lower_body');"  alt="view graph" width="20" height="20" border="0"></td></tr>

		<tr><td align="center">Flexibility (Sit & reach)</td><td align="center"> <?php echo $firstdata['flexibility'];?></td><td align="center"><?php echo $seconddata['flexibility'];?></td><td  align="center"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showgraph('graph/new.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>,'flexibility');"  alt="view graph" width="20" height="20" border="0"></td></tr>

		<tr><td align="center">Speed Test (20 meters dash)</td><td align="center"> <?php echo $firstdata['stest'];?></td><td align="center"><?php echo $seconddata['stest'];?></td><td  align="center"><img src="includes/icons/icon_view.gif" style="cursor:pointer" onclick="showgraph('graph/new.php','Graph', 1050, 550,'<?php echo $year;?>',<?php echo $sid;?>,'stest');"  alt="view graph" width="20" height="20" border="0"></td></tr>

		<tr><td width="50%" align="center"><h4>Overall Grade </h4></td><td align="center"> 
		<?php echo "<h4>".$firstdata['grade']."</h4>";?>
		</td><td align="center"><?php echo "<h4>".$seconddata['grade']."</h4>";?></td><td  align="center"></td></tr>
		</table>
		  <?php }else { echo "No assessesment";}?>
	  	 </div>
		 <div class="tab-pane bg_lg" id="3"><br>
		 
		<br>
		</div>
	       </div>
	</div>
  	
